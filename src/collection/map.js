function doubleItem(collection) {
  // Need to be implemented
  return collection.map((value) => value * 2);
}

function doubleEvenItem(collection) {
  // Need to be implemented
  return collection.filter((value) => value % 2 === 0).map((value) => value * 2);
}

function covertToCharArray(collection) {
  // Need to be implemented
  return collection.map((value) => String.fromCharCode(value + 96));
}

function getOneClassScoreByASC(collection) {
  // Need to be implemented
  return collection.filter((student) => student.class === 1).map((value) => value.score).sort((a, b) => a - b);
}

export { doubleItem, doubleEvenItem, covertToCharArray, getOneClassScoreByASC };
