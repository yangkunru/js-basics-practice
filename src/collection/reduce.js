function getMaxNumber(collection) {
  // Need to be implemented
  return collection.reduce((a, b) => (a > b ? a : b));
}

function isSameCollection(collection1, collection2) {
  // Need to be implemented
  if (collection1.length !== collection2.length) {
    return false;
  }
  return collection1.reduce((result, current, index) => {
    if (current !== collection2[index]) {
      return false;
    }
    return true;
  }, true);
}

function sum(collection) {
  // Need to be implemented
  return collection.reduce((sum, curr) => {
    return sum + curr;
  }, 0);
}

function computeAverage(collection) {
  // Need to be implemented
  return collection.reduce((accumulate, curr) => {
    return accumulate + curr;
  }) / collection.length;
}

function lastEven(collection) {
  // Need to be implemented
  return collection.reduce((result, current) => {
    return current % 2 === 0 ? current : result;
  }, null);
}

export { getMaxNumber, isSameCollection, computeAverage, sum, lastEven };
