function getAllEvens(collection) {
  // Need to be implemented
  return collection.filter((value) => value % 2 === 0);
}

function getAllIncrementEvens(start, end) {
  // Need to be implemented
  var arr = [];
  for (let i = 1; i <= 10; i++) {
    arr.push(i);
  }
  return getAllEvens(arr);
}

function getIntersectionOfcollections(collection1, collection2) {
  // Need to be implemented
  return collection1.filter((value) => collection2.includes(value));
}

function getUnionOfcollections(collection1, collection2) {
  // Need to be implemented
  return collection1.concat(collection2.filter((value) => !collection1.includes(value)));
}

function countItems(collection) {
  // Need to be implemented
  const objCount = {};
  Array.from(collection).forEach((value) => {
    objCount[value] = collection.filter((element) => element === value).length;
  });
  return objCount;
}

export {
  getAllEvens,
  getAllIncrementEvens,
  getIntersectionOfcollections,
  getUnionOfcollections,
  countItems
};
