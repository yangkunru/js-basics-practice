function flatArray(arr) {
  // Need to be implemented
  return arr.flat();
}

function aggregateArray(arr) {
  // Need to be implemented
  return arr.flatMap((item) => [[item * 2]]);
}

function getEnumerableProperties(obj) {
  // Need to be implemented
  return Object.keys(obj);
}

function removeDuplicateItems(arr) {
  // Need to be implemented
  return [...new Set(arr)];
}

function removeDuplicateChar(str) {
  // Need to be implemented
  return [...new Set(str)].join("");
}

function addItemToSet(set, item) {
  // Need to be implemented
  return set.add(item);
}

function removeItemToSet(set, item) {
  // Need to be implemented
  set.delete(item);
  return set;
}

function countItems(arr) {
  // Need to be implemented
  const map = new Map();
  Array.from(arr).forEach((value) => {
    map.set(value, arr.filter((element) => element === value).length);
  });
  return map;
}

export {
  flatArray,
  aggregateArray,
  getEnumerableProperties,
  removeDuplicateItems,
  removeDuplicateChar,
  addItemToSet,
  removeItemToSet,
  countItems
};
